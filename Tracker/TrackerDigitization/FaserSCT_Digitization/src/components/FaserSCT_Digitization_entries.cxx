#include "../SCT_Amp.h"
#include "../SCT_FrontEnd.h"
#include "../SCT_Digitization.h"
#include "../SCT_DigitizationTool.h"
#include "../SCT_SurfaceChargesGenerator.h"
#include "../SCT_RandomDisabledCellGenerator.h"
// Unused by ATLAS
// #include "../SCT_DetailedSurfaceChargesGenerator.h" 

DECLARE_COMPONENT( SCT_Amp )  
DECLARE_COMPONENT( SCT_FrontEnd )
DECLARE_COMPONENT( SCT_Digitization )
DECLARE_COMPONENT( SCT_DigitizationTool )
DECLARE_COMPONENT( SCT_SurfaceChargesGenerator )
DECLARE_COMPONENT( SCT_RandomDisabledCellGenerator )
// Unused by ATLAS
// DECLARE_COMPONENT( SCT_DetailedSurfaceChargesGenerator )


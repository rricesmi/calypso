# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.CfgGetter import addTool,addService,addAlgorithm
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationTool"          , "SCT_DigitizationTool")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_GeantinoTruthDigitizationTool"          , "SCT_GeantinoTruthDigitizationTool")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationToolHS"        , "SCT_DigitizationToolHS")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationToolPU"        , "SCT_DigitizationToolPU")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationToolSplitNoMergePU", "SCT_DigitizationToolSplitNoMergePU")
addAlgorithm("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationHS"       , "SCT_DigitizationHS")
addAlgorithm("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_DigitizationPU"       , "SCT_DigitizationPU")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getSiliconRange"               , "SiliconRange" )
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getSCT_RandomDisabledCellGenerator", "SCT_RandomDisabledCellGenerator")
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getSCT_Amp", "SCT_Amp" )
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getSCT_FrontEnd"               , "SCT_FrontEnd" )
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getPileupSCT_FrontEnd"         , "PileupSCT_FrontEnd" )
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.getSCT_SurfaceChargesGenerator", "SCT_SurfaceChargesGenerator" )
addTool("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_OverlayDigitizationTool", "SCT_OverlayDigitizationTool")
addAlgorithm("FaserSCT_Digitization.FaserSCT_DigitizationConfig.SCT_OverlayDigitization", "SCT_OverlayDigitization")

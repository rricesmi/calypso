#!/usr/bin/env python                                                                                                                                 
"""Test various ComponentAccumulator Digitization configuration modules                                                                               
                                                                                                                                                      
Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration                                                                               
"""
import sys
from AthenaCommon.Logging import log
from AthenaCommon.Constants import DEBUG
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
#from Digitization.DigitizationParametersConfig import writeDigitizationMetadata                                                                      
from FaserSCT_Digitization.FaserSCT_DigitizationConfigNew import SCT_DigitizationCfg
#from MCTruthSimAlgs.RecoTimingConfig import MergeRecoTimingObjCfg                                                                                    

# Set up logging and new style config                                                                                                                 
log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure                                                                                                                                           
ConfigFlags.Input.Files = ['g4.HITS.root']
ConfigFlags.Output.RDOFileName = "myRDO.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-MC16-SDR-14"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Concurrency.NumThreads = 1
ConfigFlags.Beam.NumberOfCollisions = 0.

ConfigFlags.GeoModel.FaserVersion = "FASER-00"               # Always needed                                                                          
ConfigFlags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01" # Always needed to fool autoconfig                                                       

ConfigFlags.lock()

# Core components                                                                                                                                     
acc = MainServicesSerialCfg()
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(PoolWriteCfg(ConfigFlags))
#acc.merge(writeDigitizationMetadata(ConfigFlags))                                                                                                    

# Inner Detector                                                                                                                                      
acc.merge(SCT_DigitizationCfg(ConfigFlags))

# Timing                                                                                                                                              
#acc.merge(MergeRecoTimingObjCfg(ConfigFlags))                                                                                                        

# Dump config                                                                                                                                         
acc.getService("StoreGateSvc").Dump = True
acc.getService("ConditionStore").Dump = True
acc.printConfig(withDetails=True)
ConfigFlags.dump()
# Execute and finish                                                                                                                                  
sc = acc.run(maxEvents=3)
# Success should be 0                                                                                                                                 
sys.exit(not sc.isSuccess())


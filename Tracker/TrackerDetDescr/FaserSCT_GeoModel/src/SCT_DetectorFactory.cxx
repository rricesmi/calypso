/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

//
// SCT_DetectorFactory: This is the top level node 
//


#include "FaserSCT_GeoModel/SCT_DetectorFactory.h" 

#include "FaserSCT_GeoModel/SCT_DataBase.h"
#include "FaserSCT_GeoModel/SCT_Identifier.h"
#include "FaserSCT_GeoModel/SCT_GeometryManager.h" 
#include "FaserSCT_GeoModel/SCT_MaterialManager.h"
#include "FaserSCT_GeoModel/SCT_GeneralParameters.h"
#include "TrackerReadoutGeometry/Version.h" 
#include "TrackerReadoutGeometry/SiCommonItems.h" 
#include "TrackerReadoutGeometry/TrackerDD_Defs.h"
#include "TrackerReadoutGeometry/SCT_ModuleSideDesign.h" 

#include "FaserSCT_GeoModel/SCT_Barrel.h"
#include "FaserSCT_GeoModel/SCT_DataBase.h"
#include "FaserSCT_GeoModel/SCT_GeoModelAthenaComps.h"

//
// GeoModel include files:
//
#include "GeoModelKernel/GeoMaterial.h"  
#include "GeoModelKernel/GeoTube.h"  
#include "GeoModelKernel/GeoLogVol.h"  
#include "GeoModelKernel/GeoNameTag.h"  
#include "GeoModelKernel/GeoIdentifierTag.h"  
#include "GeoModelKernel/GeoPhysVol.h"  
#include "GeoModelKernel/GeoVPhysVol.h"  
#include "GeoModelKernel/GeoTransform.h"  
#include "GeoModelKernel/GeoAlignableTransform.h"  
#include "GeoModelKernel/GeoShape.h"
#include "GeoModelKernel/GeoShapeUnion.h"
#include "GeoModelKernel/GeoShapeShift.h"
#include "GeoModelInterfaces/StoredMaterialManager.h"
#include "GeoModelInterfaces/IGeoDbTagSvc.h"
#include "GeoModelFaserUtilities/DecodeFaserVersionKey.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"
#include "RDBAccessSvc/IRDBRecordset.h"
#include "RDBAccessSvc/IRDBRecord.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"
#include "DetDescrConditions/AlignableTransformContainer.h"
#
#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ISvcLocator.h"

#include "GeoModelKernel/GeoDefinitions.h"
#include "GaudiKernel/SystemOfUnits.h"



#include <iostream> 
#include <iomanip> 
#include <string>
 
using TrackerDD::SCT_DetectorManager; 
using TrackerDD::SiCommonItems; 

SCT_DetectorFactory::SCT_DetectorFactory(const SCT_GeoModelAthenaComps * athenaComps,
					 const SCT_Options & options)
  : TrackerDD::DetectorFactoryBase(athenaComps),
    m_useDynamicAlignFolders(false)
{ 
  
  // Create the detector manager
  m_detectorManager = new SCT_DetectorManager(detStore());

  // Create the database
  m_db = new SCT_DataBase{athenaComps};

  // Create the material manager
  m_materials = new SCT_MaterialManager{m_db};

  // Create the geometry manager.
  m_geometryManager = new SCT_GeometryManager{m_db};
  m_geometryManager->setOptions(options);

  m_useDynamicAlignFolders = options.dynamicAlignFolders();
 
  // Set Version information
  // Get the geometry tag
  DecodeFaserVersionKey versionKey(geoDbTagSvc(),"SCT");
  IRDBRecordset_ptr switchSet
    = rdbAccessSvc()->getRecordsetPtr("SctSwitches", versionKey.tag(), versionKey.node(), "FASERDD");
  const IRDBRecord    *switches   = (*switchSet)[0];
  
  std::string layout = "Final";
  std::string description;
  if (!switches->isFieldNull("LAYOUT")) {
    layout = switches->getString("LAYOUT");
  }
  if (!switches->isFieldNull("DESCRIPTION")) {
    description = switches->getString("DESCRIPTION");
  }

  std::string versionTag = rdbAccessSvc()->getChildTag("SCT", versionKey.tag(), versionKey.node(),"FASERDD");
  std::string versionName = switches->getString("VERSIONNAME");
  int versionMajorNumber = 3;
  int versionMinorNumber = 6;
  int versionPatchNumber = 0;
  TrackerDD::Version version(versionTag,
                           versionName, 
                           layout, 
                           description, 
                           versionMajorNumber,
                           versionMinorNumber,
                           versionPatchNumber);
  m_detectorManager->setVersion(version);

} 
 
 
SCT_DetectorFactory::~SCT_DetectorFactory() 
{ 
  // NB the detector manager (m_detectorManager)is stored in the detector store by the
  // Tool and so we don't delete it.
  delete m_db;
  delete m_materials;
  delete m_geometryManager;
} 

void SCT_DetectorFactory::create(GeoPhysVol *world) 
{ 

  msg(MSG::INFO) << "Building SCT Detector." << endmsg;
  msg(MSG::INFO) << " " << m_detectorManager->getVersion().fullDescription() << endmsg;

  // Change precision.
  int oldPrecision = std::cout.precision(6);

  // The tree tops get added to world. We name it "tracker" though.
  GeoPhysVol *tracker = world;

  const SCT_GeneralParameters * sctGeneral = m_geometryManager->generalParameters();

  GeoTrf::Transform3D sctTransform = sctGeneral->partTransform("SCT");
  SCT_Barrel station("Station", m_detectorManager, m_geometryManager, m_materials);

  std::vector<std::string> partNames {"StationA", "StationB", "StationC"};
  for (int iStation = -1; iStation <= 1; iStation++)
  {
    if (sctGeneral->partPresent(partNames[iStation+1]))
    {
      m_detectorManager->numerology().addBarrel(iStation);
      SCT_Identifier id{m_geometryManager->athenaComps()->getIdHelper()};
      id.setStation(iStation);
      tracker->add(new GeoNameTag("SCT"));
      tracker->add(new GeoIdentifierTag(iStation));
      GeoAlignableTransform* stationTransform = new GeoAlignableTransform(sctTransform * sctGeneral->partTransform(partNames[iStation+1]));
      tracker->add(stationTransform);
      GeoVPhysVol* stationPhys = station.build(id);
      tracker->add(stationPhys);
      m_detectorManager->addTreeTop(stationPhys);
      m_detectorManager->addAlignableTransform(3, id.getWaferId(), stationTransform, stationPhys);
    }
  }

  // Set the neighbours
  m_detectorManager->initNeighbours();

  // Set maximum number of strips in numerology.
  for (int iDesign = 0;  iDesign <  m_detectorManager->numDesigns(); iDesign++) {
    m_detectorManager->numerology().setMaxNumPhiCells(m_detectorManager->getSCT_Design(iDesign)->cells());
  }

  // Return precision to its original value
  std::cout.precision(oldPrecision);

} 
 

const SCT_DetectorManager * SCT_DetectorFactory::getDetectorManager() const
{
  return m_detectorManager;
}
 


# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getFaserSCT_DetectorTool(name="FaserSCT_DetectorTool", **kwargs):
    kwargs.setdefault("DetectorName",     "SCT")
    kwargs.setdefault("Alignable",        True)
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc")
    kwargs.setdefault("GeometryDBSvc",    "TrackerGeometryDBSvc")
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc")
    return CfgMgr.FaserSCT_DetectorTool(name, **kwargs)


from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def FaserSCT_GeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc=acc.getPrimary()
    
    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("TrackerGeometryDBSvc"))
    
    from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    acc.addService(RDBAccessSvc("RDBAccessSvc"))

    from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    acc.addService(DBReplicaSvc("DBReplicaSvc"))

    from FaserSCT_GeoModel.FaserSCT_GeoModelConf import FaserSCT_DetectorTool
    sctDetectorTool = FaserSCT_DetectorTool()

    sctDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ sctDetectorTool ]
    
    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulateFaserSCT) or flags.Detector.OverlaySCT:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))
    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulateFaserSCT) or flags.Detector.OverlaySCT:
    #         from SCT_ConditionsAlgorithms.SCT_ConditionsAlgorithmsConf import SCT_AlignCondAlg
    #         sctAlignCondAlg = SCT_AlignCondAlg(name = "SCT_AlignCondAlg",
    #                                            UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(sctAlignCondAlg)
    #         from SCT_ConditionsAlgorithms.SCT_ConditionsAlgorithmsConf import SCT_DetectorElementCondAlg
    #         sctDetectorElementCondAlg = SCT_DetectorElementCondAlg(name = "SCT_DetectorElementCondAlg")
    #         acc.addCondAlgo(sctDetectorElementCondAlg)
    return acc

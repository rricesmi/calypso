#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"
#include "TrackerRawData/FaserSCT_RDO_Container.h"
#include "TrackerSimData/TrackerSimDataCollection.h"
#include "ScintSimEvent/ScintHitCollection.h"
#include <TH1.h>
#include <TH2.h>
#include <math.h>
#include <TProfile.h>

/* SimHit reading example - Ryan Rice-Smith, UC Irvine */

class SimHitAlg : public AthHistogramAlgorithm
{
    public:
    SimHitAlg(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~SimHitAlg();

    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();

    private:
    TH1* m_hist;  // Example histogram
    TH1* m_incAnglHist;
    TH1* m_xhist;
    TH1* m_yhist;
    TH1* m_zhist;
    TH2* m_xyhist;
    TProfile* m_hprof;
    TH1* m_rdoZhist;

    // Read handle keys for data containers
    // Any other event data can be accessed identically
    // Note the key names ("GEN_EVENT" or "SCT_Hits") are Gaudi properties and can be configured at run-time
    SG::ReadHandleKey<McEventCollection> m_mcEventKey       { this, "McEventCollection", "GEN_EVENT" };
    SG::ReadHandleKey<FaserSiHitCollection> m_faserSiHitKey { this, "FaserSiHitCollection", "SCT_Hits" };
    SG::ReadHandleKey<FaserSCT_RDO_Container> m_faserRdoKey { this, "FaserSCT_RDO_Container", "SCT_RDOs"};
    SG::ReadHandleKey<TrackerSimDataCollection> m_sctMap {this, "TrackerSimDataCollection", "SCT_SDO_Map"};
    SG::ReadHandleKey<ScintHitCollection> m_triggerHitKey {this, "ScintHitCollection", "TriggerHits"};
    SG::ReadHandleKey<ScintHitCollection> m_preshowerHitKey {this, "ScintHitCollection", "PreshowerHits"};
};
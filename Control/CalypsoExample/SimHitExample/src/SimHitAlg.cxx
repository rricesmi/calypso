#include "SimHitAlg.h"
#include "TrackerSimEvent/FaserSiHitIdHelper.h"
#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/StoreGate.h"
#include "TrackerIdentifier/FaserSCT_ID.h"


SimHitAlg::SimHitAlg(const std::string& name, ISvcLocator* pSvcLocator)
: AthHistogramAlgorithm(name, pSvcLocator) { m_hist = nullptr; }

SimHitAlg::~SimHitAlg() { }

StatusCode SimHitAlg::initialize()
{
    // initialize a histogram 
    // letter at end of TH1 indicated variable type (D double, F float etc)
    m_hist = new TH1D("Efficiency num", "Efficiency num", 19, 0, 19); //first string is root object name, second is histogram title
    m_xhist = new TH1D("x", "x", 200, -210, 210);
    m_yhist = new TH1D("y", "y", 200, -155, 155);
    m_zhist = new TH1D("z", "z", 200, -10, 10);
    m_hprof = new TProfile("IncAngleGroup", "Mean Group Size vs Incident Angle", 10, -.1, .1 ,0,5);
    m_incAnglHist = new TH1D("IncAngleHist", "Incident Angle Count", 10, -.1, .1);
    m_xyhist = new TH2D("XY Hist", "xy", 200, -210, 210, 400, -125, 125);
    m_rdoZhist = new TH1D("RDO Z Hist", "RDO Z position", 9, 0, 9);

    ATH_CHECK(histSvc()->regHist("/HIST/myhist", m_hist));
    ATH_CHECK(histSvc()->regHist("/HIST/myhistprof", m_hprof));
    ATH_CHECK(histSvc()->regHist("/HIST/myhistAngl", m_incAnglHist));
    ATH_CHECK(histSvc()->regHist("/HIST/myxhist", m_xhist));
    ATH_CHECK(histSvc()->regHist("/HIST/myyhist", m_yhist));
    ATH_CHECK(histSvc()->regHist("/HIST/myzhist", m_zhist));
    ATH_CHECK(histSvc()->regHist("/HIST/myxyhist", m_xyhist));
    ATH_CHECK(histSvc()->regHist("/HIST/myrdoZhist", m_rdoZhist));

    // initialize data handle keys
    ATH_CHECK( m_mcEventKey.initialize() );
    ATH_CHECK( m_faserSiHitKey.initialize() );
    ATH_CHECK( m_faserRdoKey.initialize());
    ATH_CHECK( m_sctMap.initialize());
    ATH_CHECK( m_triggerHitKey.initialize());
    ATH_CHECK( m_preshowerHitKey.initialize());
    ATH_MSG_INFO( "Using GenEvent collection with key " << m_mcEventKey.key());
    ATH_MSG_INFO( "Using Faser SiHit collection with key " << m_faserSiHitKey.key());
    ATH_MSG_INFO( "Using FaserSCT RDO Container with key " << m_faserRdoKey.key());
    ATH_MSG_INFO( "Using SCT_SDO_Map with key "<< m_sctMap.key());
    ATH_MSG_INFO( "Using Trigger Hit collection with key " << m_triggerHitKey.key());
    ATH_MSG_INFO( "Using Preshower Hit collection with key " << m_preshowerHitKey.key());
    return StatusCode::SUCCESS;
}

StatusCode SimHitAlg::execute()
{
    // Handles created from handle keys behave like pointers to the corresponding container
    SG::ReadHandle<McEventCollection> h_mcEvents(m_mcEventKey);
    ATH_MSG_INFO("Read McEventContainer with " << h_mcEvents->size() << " events");
    if (h_mcEvents->size() == 0) return StatusCode::FAILURE;

    SG::ReadHandle<FaserSiHitCollection> h_siHits(m_faserSiHitKey);
    ATH_MSG_INFO("Read FaserSiHitCollection with " << h_siHits->size() << " hits");

    SG::ReadHandle<FaserSCT_RDO_Container> h_sctRDO(m_faserRdoKey);
    SG::ReadHandle<TrackerSimDataCollection> h_collectionMap(m_sctMap);

    SG::ReadHandle<ScintHitCollection> h_triggerHits(m_triggerHitKey);
    SG::ReadHandle<ScintHitCollection> h_preshowerHits(m_preshowerHitKey);

    //Loop through Trig/Preshower, find those that go through Feducia region
    bool eventOk = false;
    for( const auto& trigHit : *h_triggerHits)
    {
        auto& partLink = trigHit.particleLink();
        void* trigtest = const_cast<void *>(static_cast<const void *>(partLink));
        if( trigtest == nullptr) continue;
//        std::cout<<((trigtest == nullptr) ? "trig is null" : "trig isn't null")<<std::endl;
        int pid = partLink->pdg_id();
        if( pid != 13 && pid != -13) continue;
        int trigBarcode = partLink->barcode();
        if(trigBarcode % 1000000 != 10001) continue;
        if( trigtest != nullptr && trigHit.isTrigger())
        {
            double rTrigger = sqrt(pow(trigHit.localStartPosition().x(),2) + pow(trigHit.localStartPosition().y(),2));
            if( rTrigger > 100) continue;
//            std::cout<<"x y z "<<trigHit.localStartPosition().x()<<" "<<trigHit.localStartPosition().y()<<" "<<trigHit.localStartPosition().z()<<std::endl;
/*            m_xhist->Fill(trigHit.localStartPosition().x());
            m_yhist->Fill(trigHit.localStartPosition().y());
            m_zhist->Fill(trigHit.localStartPosition().z());*/
//            m_xyhist->Fill(trigHit.localStartPosition().x(), trigHit.localStartPosition().y());
//            std::cout<<"size "<<h_preshowerHits->size()<<std::endl;
            for( const auto& preHit : *h_preshowerHits)
            {
//                preHit.print();
//                std::cout<<"1\n";            
//                std::cout<<"x y z "<<preHit.localStartPosition().x()<<" "<<preHit.localStartPosition().y()<<" "<<preHit.localStartPosition().z()<<std::endl;
                m_xhist->Fill(preHit.localStartPosition().x());
                m_yhist->Fill(preHit.localStartPosition().y());
                m_zhist->Fill(preHit.localStartPosition().z());
                auto& prePartLink = preHit.particleLink();
                void* test = const_cast<void *>(static_cast<const void *>(prePartLink));
//                std::cout<<((test == nullptr) ? "preshower is null" : "preshower isn't null")<<std::endl;
//                std::cout<<"id "<<preHit.identify()<<std::endl;
//                std::cout<<"meantime "<<preHit.meanTime()<<std::endl;
//                std::cout<<"energy loss "<<preHit.energyLoss()<<std::endl;
//                std::cout<<"2\n";
                if( test != nullptr && preHit.isPreshower() && prePartLink->pdg_id() == pid )  
                {
                    double rPreshower = sqrt(pow(preHit.localStartPosition().x(),2) + pow(preHit.localStartPosition().y(),2));
                    if( rPreshower > 100) continue;
                    int preBarcode = prePartLink->barcode();
                    if( preBarcode % 1000000 != 10001) continue;
                    eventOk = true;
//                std::cout<<"3\n";
                    m_xyhist->Fill(preHit.localStartPosition().x(), preHit.localStartPosition().y());
                    //We have now seen it hit both the trigger and preshower, want to see how many layers we registered a hit with this particle
                    //loop through RDO's, find how many layers hit with particle with same barcode

//                std::cout<<"4\n";
                }
//                std::cout<<"5\n";
            }
            if( eventOk ) break;
        }
        if( eventOk ) break;
    }
    std::cout<<" event was "<<eventOk<<std::endl;
    if( !eventOk) return StatusCode::SUCCESS;
    bool hitMatrix[3][3][2]{}; //first dim station, second plane, third sensor
    for( const auto& collection : *h_sctRDO)
    {
        for(const auto& rawdata : *collection)
        {
            //Get RDO location info from identifier
            const auto identifier = rawdata->identify();
            const FaserSCT_ID* pix;
            StoreGateSvc* detStore(nullptr);
            {
                detStore = StoreGate::pointer("DetectorStore");
                if (detStore->retrieve(pix, "FaserSCT_ID").isFailure()) { pix = 0; }
            }
            int station = pix->station(identifier);
            int plane = pix->layer(identifier);
            int row = pix->phi_module(identifier);
            int module = pix->eta_module(identifier);
            int sensor = pix->side(identifier);

//            std::cout<<"1\n";
            if( hitMatrix[station -1][plane][sensor]) continue;

//            std::cout<<"2\n";
            if( h_collectionMap->count(rawdata->identify()) == 0) continue;

//            std::cout<<"3\n";
            const auto& simdata = h_collectionMap->find(rawdata->identify())->second;
            const auto& deposits = simdata.getdeposits();
           
//            std::cout<<"4\n"; 
            bool muonFound = false;
            for( const auto& depositPair : deposits)
            {
                if( depositPair.first.isValid() && depositPair.first->barcode() % 1000000 == 10001)
                {
                    m_rdoZhist->Fill(3 * (station - 1 ) + plane);
                    muonFound = true;
                    break;
                }
            }

//            std::cout<<"5\n";
            if( !muonFound) continue;
            hitMatrix[station -1][plane][sensor] = true;

//            std::cout<<"6\n";
//                            std::cout<<"rdo station "<<station<<" plane "<<plane<<" row "<<row<<" module "<<module<<" sensor "<<sensor<<std::endl;
            //between station, plane, and sensor should be 18 hits if perfect efficiency
            //check each rdo to see if barcode matches trig/preshower barcode
            //Loop through FaserSiHits to see if we have same barcode at the same location 

//             if( !hitMatrix[station - 1][plane][sensor])
// //                            if( !hitMatrix[station-1][plane][(module > 0) ? 1 : 0])
//             {
//                 for(const auto& siHit : *h_siHits)
//                 {
//                     auto& siPartLink = siHit.particleLink();
//                     void* siLinkTest = const_cast<void *>(static_cast<const void *>(siPartLink));
//                     if(siLinkTest != nullptr
//                         && siHit.getStation() == station 
//                         && siHit.getPlane() == plane
//                         && siHit.getRow() == row
//                         && siHit.getModule() == module
//                         && siHit.getSensor() == sensor
//                         && siPartLink->barcode() % 1000000 == 10001)
//                     {
//                         //Given conditions above, should have found that we have an RDO of the same particle at same spot
//                         //Mark truth table true for this combination
//                         hitMatrix[station-1][plane][sensor] = true;
// //                                        hitMatrix[station-1][plane][(module > 0) ? 1 : 0] = true;



//                         break;
//                     }
//                 }
//             }

        }
    }
    int efficiency = 0;
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            for(int k = 0; k < 2; k++)
            {
                if( hitMatrix[i][j][k] ) efficiency++;
//                                std::cout<<hitMatrix[i][j][k]<<" ";
            }
//                            std::cout<<std::endl;
        }
//                        std::cout<<std::endl;
    }
    std::cout<<"efficiency num of "<<efficiency<<std::endl;
    m_hist->Fill(efficiency);

/*****Following is RDO work, not being used atm****
----------------------------------------------------

    //Looping through RDO's
    for( const auto& collection : *h_sctRDO)
    {
        for(const auto& rawdata : *collection)
        {
            //Use collection map to get simulation data connected to the RDO
            const auto identifier = rawdata->identify();
//            ATH_MSG_INFO("map size "<<h_collectionMap->size());
            if( h_collectionMap->count(identifier) == 0)
            {
//                ATH_MSG_INFO("no map found w/identifier "<<identifier);
                continue;
            }

            const auto& simdata = h_collectionMap->find(rawdata->identify())->second;
            const auto& deposits = simdata.getdeposits();
//            ATH_MSG_INFO("deposits size "<<deposits.size());

//            ATH_MSG_INFO("identifier: "<<rawdata->identify());
//            ATH_MSG_INFO("group size of "<<rawdata->getGroupSize());
            m_hist->Fill(rawdata->getGroupSize());

            //loop through deposits to find one w/ highest energy & get barcode
            float highestDep = 0;
            int barcode = 0;
            for( const auto& depositPair : deposits)
            {
                if( depositPair.second > highestDep)
                {
                    highestDep = depositPair.second;
                    barcode = depositPair.first->barcode();
//                    depositPair.first->print(std::cout);
                    ATH_MSG_INFO("pdg id "<<depositPair.first->pdg_id());
                }
            }
//            ATH_MSG_INFO("final barcode of: "<<barcode);

//            ATH_MSG_INFO("compact identifier "<<identifier.get_identifier32().get_compact());

            //Helper function to get hit location information from RDO identifier
            const FaserSCT_ID* pix;
            StoreGateSvc* detStore(nullptr);
            {
                detStore = StoreGate::pointer("DetectorStore");
                if (detStore->retrieve(pix, "FaserSCT_ID").isFailure()) { pix = 0; }
            }
            int station = pix->station(identifier);
            int plane = pix->layer(identifier);
            int row = pix->phi_module(identifier);
            int module = pix->eta_module(identifier);
            int sensor = pix->side(identifier);

//            ATH_MSG_INFO("trying to match hit to stat/plane/row/mod/sens: "<<station<<" "<<plane<<" "<<row<<" "<<module<<" "<<sensor);
            for (const FaserSiHit& hit : *h_siHits)
            {
//                ATH_MSG_INFO("hit w/vals "<<hit.getStation()<<" "<<hit.getPlane()<<" "<<hit.getRow()<<" "<<hit.getModule()<<" "<<hit.getSensor()<<" barcode: "<<hit.trackNumber());
                //set of conditions to confirm looking at same particle in same place for SiHit as RDO
                if(hit.getStation() == station 
                    && hit.getPlane() == plane
                    && hit.getRow() == row
                    && hit.getModule() == module
                    && hit.getSensor() == sensor
                    && hit.trackNumber() == barcode)
                {
//                    ATH_MSG_INFO("matched particle and plotting w/ barcode "<<barcode);
                    //here we plot point of angle vs countsize!
                    float delx = hit.localEndPosition().x() - hit.localStartPosition().x();
                    float dely = hit.localEndPosition().y() - hit.localStartPosition().y();
                    float delz = hit.localEndPosition().z() - hit.localStartPosition().z();
                    float norm = sqrt(delx*delx + dely*dely + delz*delz);
//                    ATH_MSG_INFO("acos val and group val of "<<acos(abs(delx)/norm)<<" "<<rawdata->getGroupSize());
                    float ang = acos(delx/norm);
                    if(ang > 1.5)
                    {
                        ang = ang - 3.1415;
                    }
                    m_hprof->Fill(ang, rawdata->getGroupSize(),1);
                    m_incAnglHist->Fill(ang);
                    break;
                }
            }
        }
    }*/

    // Since we have no pile-up, there should always be a single GenEvent in the container
//    const HepMC::GenEvent* ev = (*h_mcEvents)[0];
/*    if (ev == nullptr) 
    {
        ATH_MSG_FATAL("GenEvent pointer is null");
        return StatusCode::FAILURE;
    }*/
//    ATH_MSG_INFO("Event contains " << ev->particles_size() << " truth particles" );

    // The hit container might be empty because particles missed the wafers
//    if (h_siHits->size() == 0) return StatusCode::SUCCESS;
    
    // Loop over all hits; print and fill histogram
/*    for (const FaserSiHit& hit : *h_siHits)
    {
//        ATH_MSG_INFO("local start: "<<hit.localStartPosition().x()<<" "<<hit.localStartPosition().y()<<" "<<hit.localStartPosition().z());
//        ATH_MSG_INFO("local end: "<<hit.localEndPosition().x()<<" "<<hit.localEndPosition().y()<<" "<<hit.localEndPosition().z());
//        ATH_MSG_INFO("incident angle: "<<acos(hit.localEndPosition().x() - hit.localStartPosition().x()));
    //    hit.print();
     //   m_hist->Fill( hit.energyLoss() );
    }*/

    return StatusCode::SUCCESS;
}

StatusCode SimHitAlg::finalize()
{
    return StatusCode::SUCCESS;
}
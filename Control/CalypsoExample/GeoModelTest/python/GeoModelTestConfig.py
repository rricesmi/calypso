#!/usr/bin/env python
import sys
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def GeoModelTestCfg(flags):

    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    a = FaserGeometryCfg(flags)

    from MagFieldServices.MagFieldServicesConfig import MagneticFieldSvcCfg
    a.merge(MagneticFieldSvcCfg(flags))
    fieldSvc = a.getService("FaserFieldSvc")

    from GeoModelTest.GeoModelTestConf import GeoModelTestAlg
    a.addEventAlgo(GeoModelTestAlg())
    a.getEventAlgo("GeoModelTestAlg").FieldService = fieldSvc

    return a


if __name__ == "__main__":
    # from AthenaCommon.Logging import log, logging
    from AthenaCommon.Constants import VERBOSE, INFO
    # log.setLevel(VERBOSE)

    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = True
# Flag definition
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
# Flag specification
    ConfigFlags.GeoModel.FaserVersion     = "FASER-00"
    ConfigFlags.GeoModel.GeoExportFile    = "faserGeo.db"
    ConfigFlags.GeoModel.Align.Dynamic    = False
    ConfigFlags.Detector.SimulateVeto     = True
    ConfigFlags.Detector.SimulateFaserSCT = True
    ConfigFlags.Detector.SimulateUpstreamDipole = True
    ConfigFlags.Detector.SimulateCentralDipole = True
    ConfigFlags.Detector.SimulateDownstreamDipole = True
    ConfigFlags.lock()
    # ConfigFlags.dump()

# Configuration
    from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
    # from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

    acc = MainServicesSerialCfg()
    # acc.merge(PoolReadCfg(ConfigFlags))
    acc.merge(GeoModelTestCfg(ConfigFlags))
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    # acc.foreach_component("*").OutputLevel = VERBOSE
    # acc.foreach_component("*ClassID*").OutputLevel = INFO
    
# Execute and finish
    sys.exit(int(acc.run(maxEvents=1).isFailure()))

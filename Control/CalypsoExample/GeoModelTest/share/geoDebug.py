MessageSvc.OutputLevel = VERBOSE

from AthenaCommon.GlobalFlags import globalflags

globalflags.DetDescrVersion.set_Value_and_Lock( "FASER-00" )

#globalflags.ConditionsTag.set_Value_and_Lock( "OFLCOND-MC16-SDR-14" )

#from IOVDbSvc.CondDB import conddb
#conddb.setGlobalTag(globalflags.ConditionsTag())

#from AthenaCommon.DetFlags import DetFlags

#DetFlags.geometry.SCT_setOn()
#DetFlags.detdescr.SCT_setOn()

#include('ISF_Config/AllDet_detDescr.py')

from FaserGeoModel import SetGeometryVersion
from FaserGeoModel import GeoModelInit

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
GeoModelSvc.PrintMaterials = True
GeoModelSvc.GeoExportFile = "faserGeo.db"

#DetFlags.Print()

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from GeoModelTest.GeoModelTestConf import GeoModelTestAlg
topSequence += GeoModelTestAlg()

from AthenaCommon.AppMgr import theApp

theApp.EvtMax = 1

# Uses SQLite by default (if available)
# These lines suppress SQLite for geometry
#from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
#
#DBReplicaSvc.UseGeomSQLite = False

# This only affects conditions, not geometry
#include('PyJobTransforms/UseFrontier.py')

# Quiet some very spammy (on VERBOSE) components we don't care about
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
svcMgr.ClassIDSvc.OutputLevel = INFO
svcMgr.AthenaSealSvc.OutputLevel = INFO
svcMgr.IncidentSvc.OutputLevel = INFO

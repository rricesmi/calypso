#include "AthenaBaseComps/AthAlgorithm.h"

#include "MagFieldInterfaces/IMagFieldSvc.h"

// Minimalist algorithm to test retrieval of constructed geometry from DetStore

class GeoModelTestAlg : public AthAlgorithm
{
    public:
    GeoModelTestAlg(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~GeoModelTestAlg();

    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();

    private:  
    StatusCode testVeto();
    StatusCode testSCT();
    StatusCode testField();

    // Independently specify the "right" number of elements, for comparison with what we get from detector store
    Gaudi::Property<int>            m_numVetoStations           {this, "NumVetoStations", 2, "Number of stations in the Veto detector"};
    Gaudi::Property<int>            m_numVetoPlatesPerStation   {this, "NumVetoPlatesPerStation", 2, "Number of plates per station in the Veto detector"};
    Gaudi::Property<int>            m_numVetoPmtsPerPlate       {this, "NumVetoPmtsPerPlate", 1, "Number of pmts per plate in the Veto detector"};

    Gaudi::Property<int>            m_numSctStations            {this, "NumSCTStations", 3, "Number of stations in the SCT detector"};
    Gaudi::Property<int>            m_numSctPlanesPerStation    {this, "NumSCTPlanesPerStation", 3, "Number of planes per station in the SCT detector"};
    Gaudi::Property<int>            m_numSctRowsPerPlane        {this, "NumSCTRowsPerPlane", 4, "Number of rows of modules per plane in the SCT detector"};
    Gaudi::Property<int>            m_numSctModulesPerRow       {this, "NumSCTModulesPerRow", 2, "Number of modules per row in the SCT detector"};
    Gaudi::Property<int>            m_numSctStripsPerSensor     {this, "NumSCTStripsPerSensor", 768,"Number of readout strips per sensor in the SCT detector"};

    ServiceHandle<MagField::IMagFieldSvc> m_field { this, "FieldService", "FaserFieldSvc" };
};
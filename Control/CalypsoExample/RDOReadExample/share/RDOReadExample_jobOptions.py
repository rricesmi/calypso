from AthenaCommon.GlobalFlags import globalflags

globalflags.InputFormat.set_Value_and_Lock('pool')

import AthenaPoolCnvSvc.ReadAthenaPool

svcMgr.EventSelector.InputCollections = ["myRDO.pool.root"] #redirect file

alg = CfgMgr.RDOReadAlg() #rename func if we change file name
athAlgSeq += alg

theApp.EvtMax=-1
alg.McEventCollection = "TruthEvent" #alg.(containter type) = "(key)" ---> alg.SCT_RDO_Container = "SCT_RDOs"

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["HIST DATAFILE='rdoReadHist.root' OPT='RECREATE'"]
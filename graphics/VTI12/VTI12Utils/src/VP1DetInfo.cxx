/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/


////////////////////////////////////////////////////////////////
//                                                            //
//  Implementation of class VP1DetInfo                        //
//                                                            //
//  Author: Thomas H. Kittelmann (Thomas.Kittelmann@cern.ch)  //
//  Initial version: March 2008                               //
//                                                            //
////////////////////////////////////////////////////////////////

#include "VTI12Utils/VP1DetInfo.h"
#include "VTI12Utils/VP1JobConfigInfo.h"
#include "VTI12Utils/VP1SGAccessHelper.h"
#include "VTI12Utils/VP1SGContentsHelper.h"
#include "VP1Base/VP1Msg.h"
#include "VP1Base/IVP1System.h"
#include "VP1Base/VP1AthenaPtrs.h"

#include "GeoModelKernel/GeoPVConstLink.h"

#include "ScintReadoutGeometry/VetoDetectorManager.h"

#include "TrackerReadoutGeometry/SCT_DetectorManager.h"

#include "FaserDetDescr/FaserDetectorID.h"

#include "ScintIdentifier/VetoID.h"

#include "TrackerIdentifier/FaserSCT_ID.h"

#include "Identifier/Identifier.h"

//____________________________________________________________________
class VP1DetInfo::Imp {
public:
  template <class T>
  static const T * cachedRetrieve(const T*& cachedPtr, const char* key, const bool& configallows );

  static bool m_initialised;
  static const char m_badInitFlag;//Address of this means bad initialisation.

  static const ScintDD::VetoDetectorManager * m_vetoDetMgr;
  static const ScintDD::TriggerDetectorManager * m_triggerDetMgr;
  static const ScintDD::PreshowerDetectorManager * m_preshowerDetMgr;

  static const TrackerDD::SCT_DetectorManager * m_sctDetMgr;

  static const FaserDetectorID * m_faserIDHelper;

  static const VetoID *       m_vetoIDHelper;
  static const TriggerID *    m_triggerIDHelper;
  static const PreshowerID *  m_preshowerIDHelper;

  // static const PixelID * m_pixelIDHelper;
  static const FaserSCT_ID * m_sctIDHelper;
  // static const TRT_ID * m_trtIDHelper;
};

bool VP1DetInfo::Imp::m_initialised = false;
const char VP1DetInfo::Imp::m_badInitFlag = ' ';

const ScintDD::VetoDetectorManager * VP1DetInfo::Imp::m_vetoDetMgr = 0;
const ScintDD::TriggerDetectorManager * VP1DetInfo::Imp::m_triggerDetMgr = 0;
const ScintDD::PreshowerDetectorManager * VP1DetInfo::Imp::m_preshowerDetMgr = 0;

const TrackerDD::SCT_DetectorManager * VP1DetInfo::Imp::m_sctDetMgr = 0;

const FaserDetectorID * VP1DetInfo::Imp::m_faserIDHelper = 0;

const VetoID * VP1DetInfo::Imp::m_vetoIDHelper = 0;
const TriggerID * VP1DetInfo::Imp::m_triggerIDHelper = 0;
const PreshowerID * VP1DetInfo::Imp::m_preshowerIDHelper = 0;

const FaserSCT_ID * VP1DetInfo::Imp::m_sctIDHelper = 0;

//____________________________________________________________________
template <class T>
const T * VP1DetInfo::Imp::cachedRetrieve(const T*& cachedPtr, const char* preferredKey, const bool& configallows ) {
  const T * bad = static_cast<const T*>(static_cast<const void*>(&m_badInitFlag));
  if (cachedPtr)
    return ( cachedPtr==bad? 0 : cachedPtr );
  QString key(preferredKey);
  if (!configallows) {
    VP1Msg::messageDebug("VTI12DetInfo WARNING: Will not attempt to get (type="+QString(typeid(T).name())+", key="+key+") due to missing/disabled features in job!");
    cachedPtr = bad;
    return 0;
  }
  if (!VP1SGContentsHelper(VP1AthenaPtrs::detectorStore()).contains<T>(key)) {
    //Try to gracefully guess at a different key:
    QStringList keys = VP1SGContentsHelper(VP1AthenaPtrs::detectorStore()).getKeys<T>();
    if (keys.empty()) {
      VP1Msg::messageDebug("VTI12DetInfo WARNING: Could not find (type="+QString(typeid(T).name())+") in detector store (expected key="+key+")");
      cachedPtr = bad;
      return 0;
    }
    if (keys.count()>1) {
      VP1Msg::messageDebug("VTI12DetInfo WARNING: Could not find (type="+QString(typeid(T).name())+", key="+key+") in detector store, and could not uniquely guess at alternate key.");
      cachedPtr = bad;
      return 0;
    }
    VP1Msg::messageDebug("VTI12DetInfo WARNING: Could not find (type="+QString(typeid(T).name())+", key="+key+") in detector store. Trying with key="+keys.first()+")");
    key = keys.first();
  }
  if (!VP1SGAccessHelper(VP1AthenaPtrs::detectorStore()).retrieve(cachedPtr,key)||!cachedPtr) {
    VP1Msg::messageDebug("VTI12DetInfo WARNING: Could not retrieve (type="+QString(typeid(T).name())+", key="+key+") from detector store!");
    cachedPtr = bad;
    return 0;
  }
  VP1Msg::messageVerbose("VTI12DetInfo Succesfully retrieved (type="+QString(typeid(T).name())+", key="+key+") from detector store!");
  return cachedPtr;

}

const ScintDD::VetoDetectorManager * VP1DetInfo::vetoDetMgr() { return Imp::cachedRetrieve(Imp::m_vetoDetMgr,"Veto",VP1JobConfigInfo::hasVetoGeometry()); }
const ScintDD::TriggerDetectorManager * VP1DetInfo::triggerDetMgr() { return nullptr; }
const ScintDD::PreshowerDetectorManager * VP1DetInfo::preshowerDetMgr() {return nullptr; }
// const ScintDD::TriggerDetectorManager * VP1DetInfo::triggerDetMgr() { return Imp::cachedRetrieve(Imp::m_triggerDetMgr,"Trigger",VP1JobConfigInfo::hasTriggerGeometry()); }
// const ScintDD::PreshowerDetectorManager * VP1DetInfo::preshowerDetMgr() { return Imp::cachedRetrieve(Imp::m_preshowerDetMgr,"Preshower",VP1JobConfigInfo::hasPreshowerGeometry()); }

const TrackerDD::SCT_DetectorManager * VP1DetInfo::sctDetMgr() { return Imp::cachedRetrieve(Imp::m_sctDetMgr,"SCT",VP1JobConfigInfo::hasSCTGeometry()); }

const FaserDetectorID * VP1DetInfo::faserIDHelper() { return Imp::cachedRetrieve(Imp::m_faserIDHelper,"FaserID",true); }

const VetoID * VP1DetInfo::vetoIDHelper() { return Imp::cachedRetrieve(Imp::m_vetoIDHelper,"VetoID",VP1JobConfigInfo::hasVetoGeometry()); }
const TriggerID * VP1DetInfo::triggerIDHelper() { return nullptr; }
const PreshowerID * VP1DetInfo::preshowerIDHelper() { return nullptr; }
// const TriggerID * VP1DetInfo::triggerIDHelper() { return Imp::cachedRetrieve(Imp::m_triggerIDHelper,"TriggerID",VP1JobConfigInfo::hasTriggerGeometry()); }
// const PreshowerID * VP1DetInfo::preshowerIDHelper() { return Imp::cachedRetrieve(Imp::m_preshowerIDHelper,"PreshowerID",VP1JobConfigInfo::hasPreshowerGeometry()); }

const FaserSCT_ID * VP1DetInfo::sctIDHelper() { return Imp::cachedRetrieve(Imp::m_sctIDHelper,"FaserSCT_ID",VP1JobConfigInfo::hasSCTGeometry()); }

//____________________________________________________________________
bool VP1DetInfo::isUnsafe( const Identifier& id ) {

  const FaserDetectorID * idhelper = faserIDHelper();
  if ( !idhelper || !id.is_valid() )
    return true;

  if (idhelper->is_scint(id)) {
    if (!VP1JobConfigInfo::hasVetoGeometry() && idhelper->is_veto(id))
      return true;
    if (!VP1JobConfigInfo::hasTriggerGeometry() && idhelper->is_trigger(id))
      return true;
    if (!VP1JobConfigInfo::hasPreshowerGeometry() && idhelper->is_preshower(id))
      return true;
  }

  if (idhelper->is_tracker(id)) {
    if (!VP1JobConfigInfo::hasSCTGeometry() && idhelper->is_sct(id))
      return true;
  }
  return false;
}

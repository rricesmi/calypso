#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def FaserGeometryCfg (flags):
    acc = ComponentAccumulator()

    if flags.Detector.SimulateScintillator or flags.Detector.GeometryScintillator:
        from FaserGeoModel.ScintGMConfig import ScintGeometryCfg
        acc.merge(ScintGeometryCfg(flags))

    if flags.Detector.SimulateTracker or flags.Detector.GeometryTracker:
        from FaserGeoModel.SCTGMConfig import SctGeometryCfg
        acc.merge(SctGeometryCfg(flags))

    if flags.Detector.SimulateDipole or flags.Detector.GeometryDipole:
        from FaserGeoModel.DipoleGMConfig import DipoleGeometryCfg
        acc.merge(DipoleGeometryCfg(flags))

    return acc

To generate Calypso MC data from an installation (run) directory:

1) edit the G4FaserApp/test/runG4.py file to setup the job
....the default generator is particle-gun; it can also read EVNT files but note that the Veto is far away from (0,0,0) where most ATLAS generators put things by default
....read the comments carefully as a few other things have to be changed in the job options to switch between internal generator and generator data-file

2) source ./setup.sh
3) runG4.py

(setup.sh will put the script in your path)

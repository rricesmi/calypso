# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
#
#  The code of these services is experiment-agnostic, so we just have to configure them here for FASER instead of ATLAS
#
from G4AtlasServices.G4AtlasServicesConf import DetectorGeometrySvc, G4AtlasSvc, G4GeometryNotifierSvc
#
#  Physics region tools
#
from G4FaserTools.G4PhysicsRegionConfigNew import ScintillatorPhysicsRegionToolCfg, TrackerPhysicsRegionToolCfg #, FaserCaloPhysicsRegionToolCfg
from G4FaserServices.G4FaserServicesConf import ISF__FaserGeoIDSvc
#
#  Geometry tools
#
from G4FaserTools.G4GeometryToolConfig import MaterialDescriptionToolCfg, G4AtlasDetectorConstructionToolCfg, FASEREnvelopeCfg
#
#  Magnetic field tools - start simple
#
from G4FaserTools.G4FieldConfigNew import FASERFieldManagerToolCfg, VetoFieldManagerToolCfg
#
#  Future field managers (?)
#
# from G4FaserTools.G4FieldConfigNew import TriggerFieldManagerToolCfg, PreshowerFieldManagerToolCfg, UpstreamDipoleFieldManagerToolCfg, CentralDipoleFieldManagerToolCfg, DownstreamDipleFieldManagerToolCfg, FaserCaloFieldManagerToolCfg
from G4FaserTools.G4FieldConfigNew import TrackerFieldManagerToolCfg
#
#
def FaserGeoIDSvcCfg(ConfigFlags, name="ISF_FaserGeoIDSvc", **kwargs):
    result = ComponentAccumulator()
    # with ISF volume definitions
    result.addService(ISF__FaserGeoIDSvc(name, **kwargs))
    return result
#
def getFASER_RegionCreatorList(ConfigFlags):
    regionCreatorList = []

    from G4AtlasApps.SimFlags import simFlags
    # Settings to widen the region of study - not yet supported by FASER
    # if ConfigFlags.Beam.Type == 'cosmics' or ConfigFlags.Sim.CavernBG != 'Signal':
    #     regionCreatorList += [SX1PhysicsRegionToolCfg(ConfigFlags), BedrockPhysicsRegionToolCfg(ConfigFlags), CavernShaftsConcretePhysicsRegionToolCfg(ConfigFlags)]

    if ConfigFlags.Detector.SimulateScintillator:
        regionCreatorList += [ScintillatorPhysicsRegionToolCfg(ConfigFlags)]

    if ConfigFlags.Detector.SimulateTracker:
        regionCreatorList += [TrackerPhysicsRegionToolCfg(ConfigFlags)]

    # if ConfigFlags.Detector.SimulateFaserCalo:
    #     regionCreatorList += [FaserCaloPhysicsRegionToolCfg(ConfigFlags)]

    return regionCreatorList

#########################################################################
def FASER_FieldMgrListCfg(ConfigFlags):
    result = ComponentAccumulator()
    fieldMgrList = []

    acc   = FASERFieldManagerToolCfg(ConfigFlags)
    tool  = result.popToolsAndMerge(acc)
    fieldMgrList += [tool]

    # if ConfigFlags.Detector.SimulateUpstreamDipole:
    #     acc = UpstreamDipoleFieldManagerToolCfg(ConfigFlags)
    #     tool  = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]
    # if ConfigFlags.Detector.SimulateCentralDipole:
    #     acc = CentralDipoleFieldManagerToolCfg(ConfigFlags)
    #     tool  = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]
    # if ConfigFlags.Detector.SimulateDownstreamDipole:
    #     acc = DownstreamDipoleFieldManagerToolCfg(ConfigFlags)
    #     tool  = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]
    if ConfigFlags.Detector.SimulateVeto:
        acc = VetoFieldManagerToolCfg(ConfigFlags)
        tool  = result.popToolsAndMerge(acc)
        fieldMgrList += [tool]
    # if ConfigFlags.Detector.SimulateTrigger:
    #     acc = TriggerFieldManagerToolCfg(ConfigFlags)
    #     tool = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]
    # if ConfigFlags.Detector.SimulatePreshower:
    #     acc = PreshowerFieldManagerToolCfg(ConfigFlags)
    #     tool = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]
    if ConfigFlags.Detector.SimulateTracker:
        acc = TrackerFieldManagerToolCfg(ConfigFlags)
        tool  = result.popToolsAndMerge(acc)
        fieldMgrList += [tool]
    # if ConfigFlags.Detector.SimulateFaserCalo:
    #     acc = FaserCaloFieldManagerToolCfg(ConfigFlags)
    #     tool  = result.popToolsAndMerge(acc)
    #     fieldMgrList += [tool]

    result.setPrivateTools(fieldMgrList)
    return result

def getGeometryConfigurationTools(ConfigFlags):
    geoConfigToolList = []
    # The methods for these tools should be defined in the
    # package containing each tool, so G4FaserTools in this case
    geoConfigToolList += [MaterialDescriptionToolCfg(ConfigFlags)]
    return geoConfigToolList


def DetectorGeometrySvcCfg(ConfigFlags, name="DetectorGeometrySvc", **kwargs):
    result = ComponentAccumulator()
    kwargs.setdefault("DetectorConstruction", G4AtlasDetectorConstructionToolCfg(ConfigFlags))
    kwargs.setdefault("GeometryConfigurationTools", getGeometryConfigurationTools(ConfigFlags))

    accGeo, toolGeo = FASEREnvelopeCfg(ConfigFlags)
    kwargs.setdefault("World", toolGeo)
    result.merge(accGeo)
    kwargs.setdefault("RegionCreators", getFASER_RegionCreatorList(ConfigFlags))
    acc = FASER_FieldMgrListCfg(ConfigFlags)
    fieldMgrList = result.popToolsAndMerge(acc)
    kwargs.setdefault("FieldManagers", fieldMgrList)

    result.addService(DetectorGeometrySvc(name, **kwargs))
    return result

def G4AtlasSvcCfg(ConfigFlags, name="G4AtlasSvc", **kwargs):
    if ConfigFlags.Concurrency.NumThreads > 0:
        is_hive = True
    else:
        is_hive = False
    kwargs.setdefault("isMT", is_hive)
    kwargs.setdefault("DetectorGeometrySvc", 'DetectorGeometrySvc')
    return G4AtlasSvc(name, **kwargs)


def G4GeometryNotifierSvcCfg(ConfigFlags, name="G4GeometryNotifierSvc", **kwargs):
    kwargs.setdefault("ActivateLVNotifier", True)
    kwargs.setdefault("ActivatePVNotifier", False)
    return G4GeometryNotifierSvc(name, **kwargs)

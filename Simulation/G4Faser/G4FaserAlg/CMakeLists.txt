################################################################################
# Package: G4FaserAlg
################################################################################

# Declare the package name:
atlas_subdir( G4FaserAlg )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          GaudiKernel
                          PRIVATE
                          Control/AthenaBaseComps
                          Control/AthenaKernel
                          Control/SGTools
                          Control/StoreGate
                          DetectorDescription/GeoModel/GeoModelInterfaces
                          Event/EventInfo
                          Generators/GeneratorObjects
                          Simulation/G4Atlas/G4AtlasInterfaces
                          Simulation/G4Sim/MCTruthBase
                          Simulation/ISF/ISF_Core/ISF_Interfaces
                          Simulation/Barcode/BarcodeInterfaces)

# External dependencies:
find_package( CLHEP )
find_package( Geant4 )
find_package( HepMC )
find_package( XercesC )
find_package( Eigen )

# G4AtlasAlgLib library

#atlas_add_library( G4FaserAlgLib
#                     src/*.cxx
#                     PUBLIC_HEADERS G4AtlasAlg
#                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
# ${HEPMC_INCLUDE_DIRS}
#                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${EIGEN_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} ${HEPMC_LIBRARIES} AthenaBaseComps AthenaKernel GaudiKernel G4AtlasInterfaces SGTools StoreGateLib SGtests EventInfo GeneratorObjects MCTruthBaseLib )
#
# Component(s) in the package:
#atlas_add_component( G4FaserAlg
#                     src/components/*.cxx
#                     PUBLIC_HEADERS G4FaserAlg
#                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS}
#                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${EIGEN_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} ${HEPMC_LIBRARIES} AthenaBaseComps AthenaKernel GaudiKernel G4AtlasInterfaces G4FaserAlgLib SGTools StoreGateLib SGtests EventInfo GeneratorObjects MCTruthBaseLib )

#atlas_add_test( G4FaserAlgConfig_Test
#                SCRIPT test/runG4.py
#                PROPERTIES TIMEOUT 300 )

# Install files from the package:
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )

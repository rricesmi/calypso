#!/usr/bin/env python
if __name__ == "__main__":
    import os
    import sys
    import GaudiPython
    import ParticleGun as PG
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from AthenaCommon.AppMgr import *
    from AthenaCommon.Logging import log, logging
    from AthenaCommon.SystemOfUnits import TeV
    from AthenaCommon.PhysicalConstants import pi
    from AthenaCommon.Constants import VERBOSE, INFO
    from AthenaCommon.Configurable import Configurable
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    from G4FaserAlg.G4FaserAlgConfigNew import G4FaserAlgCfg
    from G4FaserServices.G4FaserServicesConfigNew import G4GeometryNotifierSvcCfg
    from G4AtlasTools.G4AtlasToolsConf import SensitiveDetectorMasterTool
    from G4FaserTools.G4FaserToolsConfig import generateSensitiveDetectorList
    from VetoG4_SD.VetoG4_SDToolConfig import VetoSensorSDCfg
    from FaserSCT_G4_SD.FaserSCT_G4_SDToolConfig import SctSensorSDCfg
#
# Set up logging and new style config
#
    log.setLevel(VERBOSE)
    Configurable.configurableRun3Behavior = True
#
# Input settings (Generator file)
#
#   from AthenaConfiguration.TestDefaults import defaultTestFiles
#   ConfigFlags.Input.Files = defaultTestFiles.EVNT
#
# Alternatively, these must ALL be explicitly set to run without an input file
# (if missing, it will try to read metadata from a non-existent file and crash)
#
    ConfigFlags.Input.Files = [""]
    ConfigFlags.Input.isMC = True
    ConfigFlags.Input.RunNumber = 12345
    ConfigFlags.Input.Collections = [""]
    ConfigFlags.ProjectName = "mc19"
    ConfigFlags.Common.isOnline = False
    ConfigFlags.Beam.Type = "collisions"
    ConfigFlags.Beam.Energy = 7*TeV                              # Informational, does not affect simulation
    ConfigFlags.GeoModel.FaserVersion = "FASER-00"               # Always needed
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-MC16-SDR-14"          # Always needed
# Workaround for bug/missing flag; unimportant otherwise 
    ConfigFlags.addFlag("Input.InitialTimeStamp", 0)
# Workaround to avoid failing metadata peek
    ConfigFlags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01" # Always needed to fool autoconfig
# Workaround to avoid problematic ISF code
    ConfigFlags.GeoModel.Layout = "Development"
# To be sure...
    ConfigFlags.Concurrency.NumThreads = 1
#
# Output settings
#
    ConfigFlags.Output.HITSFileName = "g4.HITS.root"
    ConfigFlags.GeoModel.GeoExportFile = "faserGeo.db" # Optional dump of geometry for browsing in vp1light
#
# Geometry-related settings
# Do not change!
#
    ConfigFlags.Detector.SimulateVeto   = True
    ConfigFlags.Detector.GeometryVeto   = True
    ConfigFlags.Detector.SimulateFaserSCT   = True
    ConfigFlags.Detector.GeometryFaserSCT   = True
    ConfigFlags.Detector.SimulateUpstreamDipole = True
    ConfigFlags.Detector.SimulateCentralDipole = True
    ConfigFlags.Detector.SimulateDownstreamDipole = True
    ConfigFlags.Detector.GeometryUpstreamDipole = True
    ConfigFlags.Detector.GeometryCentralDipole = True
    ConfigFlags.Detector.GeometryDownstreamDipole = True
    ConfigFlags.GeoModel.Align.Dynamic  = False
    ConfigFlags.Sim.ReleaseGeoModel     = False
#
# All flags should be set before calling lock
#
    ConfigFlags.lock()
#
# Construct ComponentAccumulator
#
    acc = MainServicesSerialCfg()
#
# Particle Gun generator (comment out to read generator file)
# Raw energies (without units given) are interpreted as MeV
#
    pg = PG.ParticleGun()
    pg.McEventKey = "GEN_EVENT"
    pg.randomSeed = 123456
    pg.sampler.pid = -13
    pg.sampler.mom = PG.EThetaMPhiSampler(energy=1*TeV, theta=[0, pi/20], phi=[0, 2*pi], mass=105.71)
    pg.sampler.pos = PG.PosSampler(x=[-5, 5], y=[-5, 5], z=-2100.0, t=0.0)
    acc.addEventAlgo(pg, "AthBeginSeq") # to run *before* G4
#
# Only one of these two should be used in a given job
# (MCEventSelectorCfg for generating events with no input file,
#  PoolReadCfg when reading generator data from an input file)
#    
    acc.merge(McEventSelectorCfg(ConfigFlags))
    # acc.merge(PoolReadCfg(ConfigFlags))
#
#  Output stream configuration
#
    acc.merge(OutputStreamCfg(ConfigFlags, 
                              "HITS", 
                             ["EventInfo#*",
                              "McEventCollection#TruthEvent",
                              "McEventCollection#GEN_EVENT",
                              "ScintHitCollection#*",
                              "FaserSiHitCollection#*"
                            ]))
    acc.getEventAlgo("OutputStreamHITS").AcceptAlgs = ["G4FaserAlg"]               # optional
    acc.getEventAlgo("OutputStreamHITS").WritingTool.ProcessingTag = "StreamHITS"  # required
#
#  Here is the "manual" configuration of the Geant4 pieces
#    
    acc.merge(FaserGeometryCfg(ConfigFlags))
    acc.merge(G4FaserAlgCfg(ConfigFlags))
    algo = acc.getEventAlgo("G4FaserAlg")
    veto, vetosd = VetoSensorSDCfg(ConfigFlags)
    sct, sctsd = SctSensorSDCfg(ConfigFlags)
    sendet = SensitiveDetectorMasterTool(SensitiveDetectors = [vetosd,sctsd])
    acc.addPublicTool(sendet)
    algo.SenDetMasterTool = sendet
    acc.merge(veto)
    acc.merge(sct)
    acc.addService(G4GeometryNotifierSvcCfg(ConfigFlags, ActivateLVNotifier=True))
#
# Verbosity
#
#    ConfigFlags.dump()
#    logging.getLogger('forcomps').setLevel(VERBOSE)
#    acc.foreach_component("*").OutputLevel = VERBOSE
#    acc.foreach_component("*ClassID*").OutputLevel = INFO
#    acc.getService("StoreGateSvc").Dump=True
#    acc.getService("ConditionStore").Dump=True
#    acc.printConfig(withDetails=True)
    f=open('FaserG4AppCfg_EVNT.pkl','w')
    acc.store(f)
    f.close()
#
# Execute and finish
#
    sys.exit(int(acc.run(maxEvents=1000).isFailure()))

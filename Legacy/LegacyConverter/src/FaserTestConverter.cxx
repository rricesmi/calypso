#include "LegacyConverter/FaserTestConverter.h"

#include "xAODFaserTruth/FaserTruthEventContainer.h"
#include "xAODFaserTruth/FaserTruthEventAuxContainer.h"

#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthParticleAuxContainer.h"

#include "xAODFaserTruth/FaserTruthVertexContainer.h"
#include "xAODFaserTruth/FaserTruthVertexAuxContainer.h"

#include "xAODFaserTracking/StripClusterContainer.h"
#include "xAODFaserTracking/StripClusterAuxContainer.h"

namespace Legacy {

  FaserTestConverter::FaserTestConverter (const std::string& name, ISvcLocator* pSvcLocator) : 
    AthAlgorithm( name, pSvcLocator ),
    m_inputFileName("inputFile.root"),
    m_treeName("faser"),
    m_branchName("event"),
    m_truthEventCollectionName("TruthEvent"),
    m_truthParticleCollectionName("TruthParticles"),
    m_truthVertexCollectionName("TruthVerticies"),
    m_clusCollectionName("StripClusters"),
    m_tfile(0),
    m_ttree(0),
    m_tbranch(0),
    m_faserEvent(0)
  {
    declareProperty("InputFileName", m_inputFileName);
    declareProperty("TreeName", m_treeName);
    declareProperty("BranchName", m_branchName);

    declareProperty("OutputTruthEventName", m_truthEventCollectionName);
    declareProperty("OutputTruthParticlesName", m_truthParticleCollectionName);
    declareProperty("OutputTruthVerticiesName", m_truthVertexCollectionName);

    declareProperty("OutputClustersName", m_clusCollectionName);

  }

  FaserTestConverter::~FaserTestConverter()
  {}

  StatusCode FaserTestConverter::initialize() {

    // Open old ROOT file
    ATH_MSG_INFO( "Opening file " << m_inputFileName);

    m_tfile = new TFile(m_inputFileName.c_str());
    if (m_tfile->IsZombie()) {
      ATH_MSG_FATAL( "File " << m_inputFileName << " couldn't be opened!");      
      return StatusCode::FAILURE;
    }

    // Get TTree
    m_ttree = (TTree*) m_tfile->Get(m_treeName.c_str());
    if (!m_ttree) {
      ATH_MSG_FATAL( "Tree " << m_treeName << " couldn't be found!");      
      return StatusCode::FAILURE;
    }

    // Get event branch
    m_tbranch = m_ttree->GetBranch(m_branchName.c_str());
    if (!m_tbranch) {
      ATH_MSG_FATAL( "Branch " << m_branchName << " couldn't be found!");      
      return StatusCode::FAILURE;
    }

    // Set up faserEvent
    m_faserEvent = new FaserEvent();
    m_tbranch->SetAddress(&m_faserEvent);

    // Set current event index to -1 so it will pick up first event on inrement
    m_currentEvent = -1;
    m_totalEvents = m_ttree->GetEntries();

    ATH_MSG_INFO("Tree opened with " << m_totalEvents << " events");

    return StatusCode::SUCCESS;
  }

  StatusCode FaserTestConverter::finalize()
  {
    ATH_MSG_INFO( "Closing file " << m_inputFileName);
    m_tfile->Close();
    return StatusCode::SUCCESS;
  }

  StatusCode FaserTestConverter::execute() {

    // Read in next event
    if (++m_currentEvent >= m_totalEvents) {
      ATH_MSG_WARNING( "Trying to load past total events " << m_totalEvents);
      return StatusCode::SUCCESS;
    }

    m_tbranch->GetEntry(m_currentEvent);

    ATH_MSG_INFO( "Loading event " << m_currentEvent);

    // Fill desired quantities
    ATH_CHECK(fillTruth());
    ATH_CHECK(fillTrackerClusters());

    return StatusCode::SUCCESS;
  }

  StatusCode FaserTestConverter::fillTrackerClusters() const
  {

    ATH_MSG_DEBUG( "Found " << m_faserEvent->Clusters().size()<< " tracker clusters");

    // Create the cluster containers:
    xAOD::StripClusterContainer* clusterC = new xAOD::StripClusterContainer();
    ATH_CHECK( evtStore()->record( clusterC, m_clusCollectionName ) );
    xAOD::StripClusterAuxContainer* aux = new xAOD::StripClusterAuxContainer();
    ATH_CHECK( evtStore()->record( aux, m_clusCollectionName + "Aux." ) );
    clusterC->setStore( aux );  

    // Loop over clusters in ntuple
    for (FaserCluster* clus : m_faserEvent->Clusters()) {
      xAOD::StripCluster* p = new xAOD::StripCluster();
      clusterC->push_back( p );
      p->setId(clus->Index());
      p->setGlobalPosition( clus->GlobalPosition().x(),  
			    clus->GlobalPosition().y(),
			    clus->GlobalPosition().z());

      ATH_MSG_DEBUG( "global x = " << p->globalX() << ", global y = " << p->globalY() << ", global z = " << p->globalZ() );
    }    

    return StatusCode::SUCCESS;    
  }

  StatusCode FaserTestConverter::fillTruth() const
  {

    ATH_MSG_DEBUG( "Found " << m_faserEvent->Particles().size()<< " truth particles");

    // Create the truth containers:
    xAOD::FaserTruthEventContainer* truthEvC = new xAOD::FaserTruthEventContainer();
    ATH_CHECK( evtStore()->record( truthEvC, m_truthEventCollectionName ) );
    xAOD::FaserTruthEventAuxContainer* evaux = new xAOD::FaserTruthEventAuxContainer();
    ATH_CHECK( evtStore()->record( evaux, m_truthEventCollectionName + "Aux.") );
    truthEvC->setStore( evaux );  

    xAOD::FaserTruthParticleContainer* truthPartC = new xAOD::FaserTruthParticleContainer();
    ATH_CHECK( evtStore()->record( truthPartC, m_truthParticleCollectionName ) );
    xAOD::FaserTruthParticleAuxContainer* partaux = new xAOD::FaserTruthParticleAuxContainer();
    ATH_CHECK( evtStore()->record( partaux, m_truthParticleCollectionName + "Aux." ) );
    truthPartC->setStore( partaux );  

    // Create truth particle list
    for (FaserTruthParticle* part : m_faserEvent->Particles()) {
      xAOD::FaserTruthParticle* p = new xAOD::FaserTruthParticle();
      truthPartC->push_back(p);

      p->setPdgId( part->PdgCode() );
      p->setPx( part->Momentum().x() );
      p->setPy( part->Momentum().y() );
      p->setPz( part->Momentum().z() );
      p->setE( part->Energy() );
    }

    return StatusCode::SUCCESS;
  }

} // End of namespace
